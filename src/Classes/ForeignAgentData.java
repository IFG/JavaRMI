package Classes;

import java.io.Serializable;

public class ForeignAgentData implements Serializable {

    private static final long serialVersionUID = 10L;
    private String mobileIP;
    private String coa;

    public ForeignAgentData() {
    }

    public ForeignAgentData(String mobileIP, String coa) {
        this.mobileIP = mobileIP;
        this.coa = coa;
    }

    public String getMobileIP() {
        return mobileIP;
    }

    public void setMobileIP(String mobileIP) {
        this.mobileIP = mobileIP;
    }

    public String getCoa() {
        return coa;
    }

    public void setCoa(String coa) {
        this.coa = coa;
    }

    @Override
    public String toString() {
        return "ForeignAgentData{" + "mobileIP=" + mobileIP + ", coa=" + coa + '}';
    }

}
