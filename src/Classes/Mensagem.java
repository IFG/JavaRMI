package Classes;

public class Mensagem {

    private String mensagem;

    public Mensagem() {
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    @Override
    public String toString() {
        return "Mensagem{" + "mensagem=" + mensagem + '}';
    }

}
