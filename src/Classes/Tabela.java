package Classes;

import java.util.ArrayList;

public class Tabela {

    private ArrayList<String> mobileIps = new ArrayList<String>();

    public Tabela() {
    }

    public ArrayList<String> getMobileIp() {
        return mobileIps;
    }

    public void addMobileIps(ArrayList<String> mobileIp) {
        this.mobileIps = mobileIp;
    }

    public void addMobileIp(String mobileIp) {
        this.mobileIps.add(mobileIp);
    }

    @Override
    public String toString() {
        return "Tabela{" + "mobileIp=" + mobileIps + '}';
    }

}
