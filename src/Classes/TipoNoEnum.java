package Classes;

public enum TipoNoEnum {

    NO_FIXO("No Fixo"), NO_MOVEL("N Móvel");

    private final String tipoNo;

    TipoNoEnum(String tipoNo) {
        this.tipoNo = tipoNo;
    }

    public String getTipoNo() {
        return tipoNo;
    }

}
