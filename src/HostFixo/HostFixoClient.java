package HostFixo;

import Classes.Mensagem;
import Interface.ConstantHome;
import Interface.InterHomeAgent;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.RemoteException;
import javax.swing.JOptionPane;

public class HostFixoClient {

    static Mensagem mensagem = new Mensagem();

    public HostFixoClient() {
    }

    private static void sendMessage() throws RemoteException {
        HostFixoClient.mensagem.setMensagem(JOptionPane.showInputDialog("Insira a Mensagem que deseja enviar"));
        System.out.println("Mensagem para HomeAgent: " + HostFixoClient.mensagem.getMensagem());
    }

    public static void main(String args[]) throws RemoteException {
        try {
            sendMessage();
            Registry registry = LocateRegistry.getRegistry("localhost", ConstantHome.RMI_PORT);
            final InterHomeAgent interHomeAgent = (InterHomeAgent) registry.lookup(ConstantHome.RMI_ID);
            interHomeAgent.sendMessage(HostFixoClient.mensagem.getMensagem());
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
