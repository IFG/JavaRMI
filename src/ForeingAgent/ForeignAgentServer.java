package ForeingAgent;

import Interface.ConstantForeign;
import Interface.ConstantMovel;
import Interface.InterForeignAgent;
import Interface.InterMobileNode;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ForeignAgentServer implements InterForeignAgent {

    public ForeignAgentServer() {
    }

    public static void main(String args[]) throws RemoteException {
        try {
            ForeignAgentServer foreignAgentServer = new ForeignAgentServer();
            InterForeignAgent interForeignAgent = (InterForeignAgent) UnicastRemoteObject.exportObject(foreignAgentServer, 0);
            Registry registry = LocateRegistry.createRegistry(ConstantForeign.RMI_PORT);
            registry.bind(ConstantForeign.RMI_ID, interForeignAgent);
            System.out.println("Server ready!");

        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessage(String message, String ipMobile) throws RemoteException {
        try {
            System.out.println("Mensagem do HomeAgent: " + message);

            Registry registry = LocateRegistry.getRegistry(ipMobile, ConstantMovel.RMI_PORT);
            final InterMobileNode interMobileNode = (InterMobileNode) registry.lookup(ConstantMovel.RMI_ID);
            interMobileNode.sendMessage(message);
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

}
