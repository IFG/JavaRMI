package ForeingAgent;

import Classes.ForeignAgentData;
import Classes.Tabela;
import HostMovel.HostMovelServer;
import Interface.ConstantHome;
import Interface.ConstantMovel;
import Interface.InterHomeAgent;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import Interface.InterMobileNode;
import javax.swing.JOptionPane;

public class ForeignAgentClient {

    public ForeignAgentClient() {
    }

    private static String MyIP() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
            Logger.getLogger(HostMovelServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void main(String args[]) {
        try {
            Tabela tabela = new Tabela();
            ForeignAgentData foreingAgentData = new ForeignAgentData();

            int respost = JOptionPane.showConfirmDialog(null, "Deseja Iniciar a Operação ?", "Alerta", JOptionPane.YES_NO_OPTION);
            if (respost == 0) {
                Registry registry = LocateRegistry.getRegistry("localhost", ConstantMovel.RMI_PORT);
                final InterMobileNode remote = (InterMobileNode) registry.lookup(ConstantMovel.RMI_ID);
                String mobileIP = remote.getMobileIP();

                tabela.addMobileIp(mobileIP);

                System.out.println("IP do nó móvel: " + mobileIP);
                System.out.println(tabela.toString());

                foreingAgentData.setMobileIP(mobileIP);
                foreingAgentData.setCoa(MyIP());

                System.out.println(foreingAgentData.toString());

                Registry registry2 = LocateRegistry.getRegistry("localhost", ConstantHome.RMI_PORT);
                final InterHomeAgent remoteHomeAgent = (InterHomeAgent) registry2.lookup(ConstantHome.RMI_ID);
                remoteHomeAgent.sendForeignAgentData(foreingAgentData);
            } else {
                System.out.println("Operação Cancelada!");
            }
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }

}
