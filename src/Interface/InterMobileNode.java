package Interface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterMobileNode extends Remote {

    String getMobileIP() throws RemoteException;

    void sendMessage(String message) throws RemoteException;

}
