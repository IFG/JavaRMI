package Interface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterForeignAgent extends Remote {

    void sendMessage(String message, String ipMobile) throws RemoteException;

}
