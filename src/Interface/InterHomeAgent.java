package Interface;

import java.rmi.RemoteException;
import Classes.ForeignAgentData;
import java.rmi.Remote;

public interface InterHomeAgent extends Remote {

    void sendForeignAgentData(ForeignAgentData foreignAgentData) throws RemoteException;

    void sendMessage(String message) throws RemoteException;

}
