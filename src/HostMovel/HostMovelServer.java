package HostMovel;

import Interface.ConstantMovel;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import Interface.InterMobileNode;
import java.rmi.RemoteException;

public class HostMovelServer implements InterMobileNode {

    public HostMovelServer() {
    }

    @Override
    public String getMobileIP() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException ex) {
            Logger.getLogger(HostMovelServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void sendMessage(String message) throws RemoteException {
        System.out.println("Mensagem do ForeignAgent: " + message);
    }

    public static void main(String args[]) {
        try {
            HostMovelServer foreignAgentServer = new HostMovelServer();
            InterMobileNode mensagemInterface = (InterMobileNode) UnicastRemoteObject.exportObject(foreignAgentServer, 0);
            Registry registry = LocateRegistry.createRegistry(ConstantMovel.RMI_PORT);
            registry.bind(ConstantMovel.RMI_ID, mensagemInterface);
            System.out.println("Nó móvel inicializado!");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

}
