package HomeAgent;

import Classes.ForeignAgentData;
import Interface.ConstantForeign;
import Interface.ConstantHome;
import Interface.InterForeignAgent;
import Interface.InterHomeAgent;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class HomeAgentServer implements InterHomeAgent {

    ForeignAgentData foreignAgentData = new ForeignAgentData();

    public HomeAgentServer() {
    }

    @Override
    public void sendForeignAgentData(ForeignAgentData foreignAgentData) throws RemoteException {
        System.out.println("Informações do AgentForeign: " + foreignAgentData.toString());
        this.foreignAgentData = foreignAgentData;
    }

    @Override
    public void sendMessage(String message) throws RemoteException {
        try {
            System.out.println("Mensagem do HostFixo: " + message);

            Registry registry = LocateRegistry.getRegistry(foreignAgentData.getCoa(), ConstantForeign.RMI_PORT);
            final InterForeignAgent interForeignAgent = (InterForeignAgent) registry.lookup(ConstantForeign.RMI_ID);
            interForeignAgent.sendMessage(message, foreignAgentData.getMobileIP());
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

    public static void main(String args[]) throws RemoteException {
        try {
            HomeAgentServer homeAgentServer = new HomeAgentServer();
            InterHomeAgent mensagemInterface = (InterHomeAgent) UnicastRemoteObject.exportObject(homeAgentServer, 0);
            Registry registry = LocateRegistry.createRegistry(ConstantHome.RMI_PORT);
            registry.bind(ConstantHome.RMI_ID, mensagemInterface);

            System.out.println("Server ready!");

        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

}
